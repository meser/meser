VERSION = (0, 0, 0, 'alpha', 0)


def get_version(*args, **kwargs):
    # Only import if it's actually called.
    from meser.utils.version import get_version
    return get_version(*args, **kwargs)