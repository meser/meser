import threading

from meser.core.app import Application
from meser.contrib.stream import Stream
from meser.contrib.stream import exceptions


class BaseApplication(object):
    """
    """

    # _instance_lock = threading.Lock()
    #
    # @staticmethod
    # def instance():
    #     if not hasattr(BaseApplication, "_instance"):
    #         with BaseApplication._instance_lock:
    #             if not hasattr(BaseApplication, "_instance"):
    #                 # New instance after double check
    #                 BaseApplication._instance = BaseApplication()
    #     return BaseApplication._instance

    def __init__(self, name):
        self.core = Application.instance()