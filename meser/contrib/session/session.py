import uuid
import time
import weakref
import datetime

import tornadoredis

import tornado
import tornado.ioloop
import tornado.gen
import tornado.web


class SessionsDict:
    def __init__(self, app, *args, **kwargs):
        """
        @param app meser.core.app.Application
        """
        self._app = app
        self.client = None

        # Run GC for dead sessions
        # self._cleaner = tornado.ioloop.PeriodicCallback(self.remove_dead_sessions, 5000)
        # self._cleaner.start()

        # For debug reason
        self.connect()

    def connect(self, host='localhost', port=6379, unix_socket_path=None,
             password=None, selected_db=None, io_loop=None, connection_pool=None):
        self.client = tornadoredis.Client(host, port, unix_socket_path,
                 password, selected_db, io_loop, connection_pool)
        self.client.connect()

    def remove_dead_sessions(self):
        # dead_seassions = [x for x in self.keys() if self[x].dead]
        # for dead in dead_seassions:
        #     del self[dead]
        pass

    def new(self, request):
        uid = str(uuid.uuid4())
        existed = self.get(uid)
        print existed
        if not existed:
            session = Session(uid, self.client)
            session.set_request(request)
            return session
        else:
            # I think we newer get an exception with non unique UID =)
            return self.new(request)

    @tornado.gen.engine
    def get(self, uid):
        ctime = yield tornado.gen.Task(self.client.get,  'session:%s:ctime' % uid)
        if ctime:
            raise tornado.gen.Return(Session(uid, self.client))
        else:
            raise tornado.gen.Return(None)


class Session:
    def __init__(self, uid, redis_client):
        self._id = uid
        self._key = 'session:%s' % self.uid
        self._rc = redis_client

        self._ctime = time.time()
        self._mtime = self.start

        self._host = ''
        self._protocol = ''
        self._ip = ''

        self._alive = True

        self._meta = {}

    @tornado.gen.engine
    def _load(self):
        self._ctime = self._get('ctime', time.time())
        self._mtime = self._get('mtime', self.start)

    def __del__(self):
        print('Deleting %s' % self)

    def set_request(self, request):
        pass

    # ######################################
    # Properties
    # ######################################
    @property
    def uid(self):
        return self._id

    @property
    def duration(self):
        return time.time()-self.start

    @property
    def start(self):
        return self._ctime

    @property
    def dead(self):
        if time.time() - self._mtime < 60:
            return not self._alive
        return True

    @property
    def alive(self):
        return not self.dead

    @property
    def meta(self):
        return self._meta

    # ######################################
    # Public functions
    # ######################################
    def touch(self):
        self._mtime = time.time()
        pipe = self._rc.pipeline()
        # pipe.

    def reject(self):
        self.touch()
        self._alive = False
        self.rejected = True

    def delete(self):
        pass

    # ######################################
    # Magic-functions
    # ######################################
    def __repr__(self):
        return '<Session {protocol} {uid} {ip} {status}>'.format(
            protocol=self._protocol, uid=self.uid, 
            ip=self._ip, status=('alive' if self.alive else 'dead'))

    def __eq__(self, other):
        return self.uid == other.uid

    @tornado.gen.engine
    def _set(self, key, value):
        r_key = '%s:%s' % (self._key, key)
        status = yield tornado.gen.Task(self._rc.set, r_key, value)
        raise tornado.gen.Return(value=status)

    @tornado.gen.engine
    def _get(self, key, default=None):
        r_key = '%s:%s' % (self._key, key)
        value = yield tornado.gen.Task(self._rc.get, r_key)
        if value:
            raise tornado.gen.Return(value=value)
        elif default:
            r = yield self._set(key, default)
            if r:
                raise tornado.gen.Return(value=default)
            else:
                raise Exception('Can\'t write default value')
        else:
            raise tornado.gen.Return(value=None)