class StreamNameExists(Exception):
    pass


class StreamDoesNotExists(Exception):
    pass


class StreamSeemsToBeBroken(Exception):
    pass