from meser.contrib.stream.frame.base import BaseFrame


class AudioFrame(BaseFrame):
    def __init__(self, data):
        super(AudioFrame, self).__init__(data)