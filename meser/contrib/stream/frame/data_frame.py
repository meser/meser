from meser.contrib.stream.frame.base import BaseFrame


class DataFrame(BaseFrame):
    def __init__(self, data):
        super(DataFrame, self).__init__(data)