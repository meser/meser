from meser.contrib.stream.frame.base import BaseFrame


class VideoFrame(BaseFrame):
    def __init__(self, data):
        super(VideoFrame, self).__init__(data)