from meser.core.app import Application
from meser.contrib.stream import Stream
from meser.contrib.stream import exceptions


class StreamManager(object):
    """
    StreamManager - special service.
    Initiates ones per application.
    """
    streams = {}

    # _instance_lock = threading.Lock()
    #
    # @staticmethod
    # def instance():
    #     if not hasattr(StreamManager, "_instance"):
    #         with StreamManager._instance_lock:
    #             if not hasattr(StreamManager, "_instance"):
    #                 # New instance after double check
    #                 StreamManager._instance = StreamManager()
    #     return StreamManager._instance

    def __init__(self, application=None):
        self.core = Application.instance()
        self.app = application or None

    def create(self, name):
        if name in self.streams:
            raise exceptions.StreamNameExists("Stream with name `{}` already exists in this application.".format(name))
        self.streams[name] = Stream(self)
        return self.streams[name]

    def cleanup(self, name, force=False):
        """
        Cleanups stream data.
        if `force` removes all data from stream
        """
        if name not in self.streams:
            raise exceptions.StreamDoesNotExists("Stream `{}` does not exists".format(name))

        # @TODO: Make cleaner
        raise NotImplementedError

    def __getattr__(self, item):
        if item not in self.streams:
            raise exceptions.StreamDoesNotExists("Stream `{}` does not exists".format(item))

        return self.streams[item]

    __getitem__ = __getattr__

