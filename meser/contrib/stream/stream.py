from meser.utils.signal import Signal


class Stream:
    def __init__(self):
        self._frames = []
        # self.core = Application.instance()
        self.update = Signal()
        self._maxframes = None
        self._maxtime = None

    def push(self, frame):
        # @TODO: Delete old frames
        self._frames.append(frame)

        if self.maxframes > 0 and len(self._frames) > self.maxframes:
            self._frames.pop(0)

        self.update(frame)

    @property
    def maxframes(self):
        return self._maxframes or 0

    @maxframes.setter
    def maxframes(self, v):
        self._maxframes = v
