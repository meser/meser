import time
import threading

import tornado.httpserver
import tornado.process
import tornado.netutil
import tornado.ioloop
import tornado.wsgi
import tornado.web

from meser.contrib.session import SessionsDict


class Application():
    _config = None

    _instance_lock = threading.Lock()

    @staticmethod
    def instance():
        if not hasattr(Application, "_instance"):
            with Application._instance_lock:
                if not hasattr(Application, "_instance"):
                    # New instance after double check
                    Application._instance = Application()
        return Application._instance

    def __init__(self):
        if self._config: return
        self.configure()

        # Set start time
        self._start_time = time.time()

        # For HTTP Handlers
        self.application = None

        # Empty server list
        self.servers = []
        self.configurations = {}

        # Tasks
        self._tasks = {}

        # Empty sessions
        self.sessions = SessionsDict(self)

        # IOLoop
        self._ioloop = None

        # Create router for HTTP-based protocols
        self._create_router()

    def configure(self):
        if not self._config:
            self._config = {'a': 1}

    # ######################
    # Protected methods
    # ######################
    def _create_router(self):
        if hasattr(self, 'router'):
            del self.router
        self.router = tornado.web.Application()

        from meser.protocols.hls.handler import PlaylistHandler, ChunkHandler

        self.router.add_handlers(
            'dev\.homny\.ru', (
                (r'/(?P<stream_name>\w+)/(?P<playlist_name>[0-9a-zA-Z_-]{1,16})(?P<extension>\.m3u[8]{0,1})',
                 PlaylistHandler),
                (
                r'/(?P<stream_name>\w+)/(?P<session>[a-z0-9-]{36})/(?P<playlist_name>[0-9a-zA-Z_-]{1,4})(?P<extension>\.m3u[8]{0,1})',
                PlaylistHandler),
                (
                r'/(?P<stream_name>\w+)/(?P<session>[a-z0-9-]{36})/(?P<chunk>[a-z0-9]{1,}).(?P<extension>mp4|ts|mpeg|mpegts)',
                ChunkHandler),
            ))

        from meser.protocols.static.handler import StaticFileHandler

        self.router.add_handlers(
            'localhost', (
                (r'/root/(?P<path>.*)', StaticFileHandler, {
                    'webroot': '',
                    'origin': 'http://cdnjs.cloudflare.com/',
                    'root': '/tmp/teststatic'
                }),
            ))

    # ######################
    # Public methods
    # ######################
    def start(self):
        sockets = tornado.netutil.bind_sockets(8000, '127.0.0.1')

        if not self._ioloop:
            # tornado.process.fork_processes(tornado.process.cpu_count())
            # from zmq.eventloop import ioloop
            # ioloop.install()
            self._ioloop = tornado.ioloop.IOLoop.instance()

        server = tornado.httpserver.HTTPServer(self.router)
        server.add_sockets(sockets)

        self.servers.append(server)
        self._ioloop.start()

    def start_task(self, task, name=None, interval=None):
        if not interval:
            self._ioloop.add_callback(task)
        else:
            periodic = tornado.ioloop.PeriodicCallback(task, interval)
            self._tasks[(name or periodic.__hash__())] = periodic
            periodic.start()

    def stop(self):
        self._ioloop.stop()
        for i in range(len(self.servers)):
            self.servers.remove(i)

    def uptime(self):
        return time.time() - self._start_time