import logging


class Object(object):
    def __new__(cls, *args, **kwargs):
        o = super(Object, cls).__new__(cls)
        o.log = logging.getLogger(cls)
        return o