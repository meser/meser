import tornado.web
import tornado.gen

from meser.core.app import Application
from meser.core.defaults.handlers import HttpRequestHandler


class ControlApplication(tornado.web.Application):
    pass


class ControlHandler(HttpRequestHandler):
    """
    Base class for all control handlers (methods).
    """
    pass


class ShutdownMethod(ControlHandler):
    """
    Shutdown server
    """

    @tornado.gen.coroutine
    def get(self, *args, **kwargs):
        Application.instance().stop()
