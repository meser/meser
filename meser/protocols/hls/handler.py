import tornado.web
import tornado.gen

from . import m3u8
from .m3u8.model import StreamInfo
from meser.core.app import Application
from meser.core.defaults.handlers import HttpRequestHandler


# .m3u8 application/x-mpegURL application/vnd.apple.mpegurl
# .ts video/MP2T

class PlaylistHandler(HttpRequestHandler):
    @tornado.gen.coroutine
    def get(self, stream_name, playlist_name, session=None, *args, **kwargs):
        # Start a new or get a session from uid
        if playlist_name == 'index':
            session = Application.instance().sessions.get(session) or Application.instance().sessions.new(self.request)
        else:
            session = Application.instance().sessions.get(session)

        # If session is dead or not started send NotAllowed error
        if not session or session.dead:
            raise tornado.web.HTTPError(403)

        # Touch it ;)
        session.touch()

        # Setting up content-type header
        # self.set_header('Content-type', 'audio/x-mpegurl')  # US-ASCII
        self.set_header('Content-type', 'application/vnd.apple.mpegurl')  # UTF-8

        print('Playlist', session, stream_name, playlist_name)

        m3u = m3u8.M3U8()
        m3u.version = 3

        if playlist_name == 'index':
            for i in range(10):
                si = StreamInfo('4800', '1', '3x3', '')
                pl = m3u8.Playlist('{}.m3u8'.format(i), si, '/')
                m3u.add_playlist(pl)
        else:
            for i in range(5):
                segment = m3u8.Segment('{}.ts'.format(i), duration=10)
                m3u.segments.append(segment)

        m3u.base_path = m3u.base_uri = "http://{host}/{stream_name}/{session}". \
            format(
            host=self.request.host,
            stream_name=stream_name,
            session=session.uid)

        self.finish(m3u.dumps())


class ChunkHandler(HttpRequestHandler):
    @tornado.gen.coroutine
    def get(self, stream_name, session, chunk, extension, **kwargs):
        session = Application.instance().sessions.get(session)
        if not session or session.dead:
            raise tornado.web.HTTPError(403)
        session.touch()

        self.set_header('Content-type', 'video/MP2T')

        print('Chunk', session, stream_name, chunk)
        self.finish()
