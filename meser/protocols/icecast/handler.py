import base64
import copy

import tornado.web
from tornado import httputil
from tornado.tcpserver import TCPServer
from tornado.escape import parse_qs_bytes

from meser.core.defaults.handlers import HttpRequestHandler
from meser.formats import mp3


class RadioControlServer(TCPServer):
    def __init__(self, io_loop=None, ssl_options=None, **kwargs):
        TCPServer.__init__(self, io_loop=io_loop, ssl_options=ssl_options, **kwargs)

    def handle_stream(self, stream, address):
        # @TODO: Applications map to stream multiply icecast streams
        # RadioControlProtocol(stream, address)
        RadioIcecastControlProtocol(stream, address)


class RadioIcecastControlProtocol(object):
    def __init__(self, stream, address):
        """
        @param stream tornado.iostream.BaseIOStream
        @param address tuple
        """
        self.stream = stream
        self.address = address

        self.password = '123'

        self.method, self.uri, self.version = None, None, None
        self.headers = None
        self._authenticated = False

        self.record = open('/tmp/record.mp3', 'wb')
        self.buffer = b""

        self.stream.set_close_callback(self.on_close)
        self.stream.read_until(b'\r\n', self.read_method)

    def check_password(self, password):
        """
        Old protocol
        """
        password = password.rstrip(r'\r\n')
        if password == self.password:
            self.stream.write('OK2\r\n')
            self.stream.write('icy-caps:11\r\n\r\n')

            self._authenticated = True
            self.stream.read_until(b'\r\n\r\n', self.read_headers)
        else:
            self.stream.write('%s\r\n' % password)
            self.stream.read_until(b'\r\n', self.check_password)

    def auth(self):
        if self._authenticated:
            return True

        if 'Authorization' in self.headers and 'Basic' in self.headers['Authorization']:
            username, password = base64.b64decode(self.headers['Authorization'].split(b' ')[1]).split(':')
            return password == self.password

        return False

    def read_method(self, headers):
        if b' ' not in headers:
            self.check_password(headers)
            return

        self.method, self.uri, self.version = headers.rstrip(r'\r\n').split(b' ')
        self.uri = str(self.uri)
        self.stream.read_until(b'\r\n\r\n', self.read_headers)

    def read_headers(self, headers):
        self.headers = httputil.HTTPHeaders.parse(headers)

        self.path, sep, self.query = self.uri.partition('?')
        self.arguments = parse_qs_bytes(self.query, keep_blank_values=True)
        self.query_arguments = copy.deepcopy(self.arguments)

        if self.method == 'SOURCE':
            self.source()

        elif self.method == 'GET':
            self.get()

    def source(self):
        if self.auth():
            self.stream.write(b'HTTP/1.0 200 OK\r\n\r\n')
            # self.stream.write(b'ICY 200 OK\r\n\r\n')
            self.stream.read_until_close(self.read_mp3_stream, self.read_mp3_stream)
        else:
            self.stream.write(b'HTTP/1.0 401 Unauthorized\r\n\r\n')
            self.stream.close()

    def get(self):
        if self.auth():
            if not self.stream.closed():
                # All is OK, auth pass
                self.stream.write(b"\r\n".join((b"HTTP/1.0 200 OK", b"Content-Type: text/xml", b"Content-Length: 113")))
                self.stream.write(b"\r\n\r\n")
                self.stream.write(b"\r\n".join(
                    (b"<?xml version=\"1.0\"?>",
                     b"<iceresponse><message>Metadata update successful</message><return>1</return></iceresponse>")))
                self.stream.close()
        else:
            self.stream.write(b'HTTP/1.0 401 Unauthorized\r\n\r\n')
            self.stream.close()

    def read_mp3_stream(self, data):
        """
        @param data bytes
        """
        self.buffer += data

        good_index = 0
        index = 0
        max = len(self.buffer)

        while index < max - 4:
            good, length = 0, 1
            if self.buffer.startswith('TAG', index):
                # ID3v1 tag
                good = 1
                length = 128

            elif self.buffer.startswith('ID3', index) and max - index > 9:
                # IV3v2 tag
                good = 1
                length = (ord(self.buffer[index + 6]) << 21) + \
                         (ord(self.buffer[index + 7]) << 14) + \
                         (ord(self.buffer[index + 8]) << 7) + \
                         (ord(self.buffer[index + 9]) + 10)

            elif self.buffer[index] == '\xff' and \
                    not self.buffer[index + 1] == '\xff' and \
                    (ord(self.buffer[index + 1]) & 224) == 224:
                # MP3 frames
                try:
                    hdr = mp3.frameheader(self.buffer, index)
                    length = mp3.framelen(hdr)
                    good = 1
                except mp3.MP3Error, e:
                    pass

            if good:
                if index + length <= max:
                    good_index = index + length
                    self.on_frame(self.buffer[index:good_index])

            index = index + length
        self.buffer = self.buffer[good_index:]

    def on_frame(self, frame):
        self.record.write(frame)

    def on_close(self):
        print("Stream is closed")
        self.record.close()


class IcecastStreamHandler(HttpRequestHandler):
    def initialize(self, stream):
        self.stream = stream


if __name__ == '__main__':
    import tornado.process
    import tornado.netutil
    import tornado.ioloop

    IOLoop = tornado.ioloop.IOLoop.instance()

    import signal

    def handle_signal(signal, frame):
        print "Exiting"
        IOLoop.add_callback_from_signal(lambda: IOLoop.current().stop())

    signal.signal(signal.SIGHUP, handle_signal)
    signal.signal(signal.SIGINT, handle_signal)
    signal.signal(signal.SIGTERM, handle_signal)

    server = RadioControlServer(IOLoop)
    server.listen(8800, '0.0.0.0')

    IOLoop.start()