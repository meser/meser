import socket

import tornado.web
import tornado.gen
import tornado.iostream
from tornado.httputil import HTTPHeaders
from tornado.util import GzipDecompressor
import re

from meser.formats import mp3
from meser.contrib.stream import Stream
from meser.contrib.stream.frame import AudioFrame


try:
    # Python 3
    from urllib import parse
except ImportError:
    # Python 2
    from urlparse import urlparse as parse


class IceReceiver(object):
    def __init__(self, uri):
        self._uri = uri

        self._socket = socket.socket()
        self._interface = None
        self._decompressor = None

        self._record_to = None

        self._net_stream = None

        self._meta = {
            'genre': '',
            'name': '',
            'url': '',
            'description': ''
        }

        self.stream = Stream()
        self.stream.maxframes = 10

        self._buffer = b""
        self._data_buffer = b""

        self._bitrate = 0
        self._channels = 0
        self._samplerate = 44100

        self._use_meta = False

        self._metaint = 0
        self._totalbytes = 0

        self._chunk_size = 4096

    @property
    def interface(self):
        return self._interface

    @interface.setter
    def interface(self, interface):
        if not self._interface:
            self._interface = interface
            self._socket.bind(self._interface)

    @property
    def meta(self):
        return self._meta

    @meta.setter
    def meta(self, meta):
        if len(meta):
            self._meta.update(meta)

            print "Meta:"
            for i in self._meta.items():
                print "%15s : %r" % i

    def use_meta(self, really=False):
        self._use_meta = really or False

    def start(self):
        uri = parse(self._uri)

        host = uri.netloc
        port = 80
        if ':' in uri.netloc:
            host, port = uri.netloc.split(':', 1)

        self._socket.connect((host, int(port)))

        path = uri.path
        if uri.query:
            path += "?%s" % uri.query

        self._net_stream = tornado.iostream.IOStream(self._socket)
        self._net_stream.set_close_callback(self._on_close)

        self._net_stream.write("GET %s HTTP/1.1\r\n" % path)
        self._net_stream.write("Accept: */*\r\n")
        self._net_stream.write("Accept-encoding: gzip, deflate, compress\r\n")
        self._net_stream.write("Host: %s\r\n" % uri.netloc)
        self._net_stream.write("User-Agent: MeSer/0.0.0\r\n")
        if self._use_meta:
            self._net_stream.write("Icy-MetaData: 1\r\n")
        self._net_stream.write("\r\n")

        self._net_stream.read_until_regex(b"\r?\n\r?\n", self._on_headers)

    def stop(self):
        self._net_stream.close()

    def record(self, fd):
        self._record_to = fd
        self.stream.update.connect(self._write_to_fd)

    def _write_to_fd(self, frame):
        if self._record_to:
            self._record_to.write(frame.data)

    def _on_close(self):
        if self._record_to:
            self._record_to.close()

    @tornado.gen.coroutine
    def _on_headers(self, data):
        first_line, _, header_data = data.partition("\n")
        match = re.match("HTTP/1.[01] ([0-9]+) ([^\r]*)", first_line)
        assert match
        code = int(match.group(1))

        self.headers = HTTPHeaders.parse(header_data)
        if 100 <= code < 200:
            return
        else:
            self.code = code
            self.reason = match.group(2)

        if "Content-Length" in self.headers:
            if "," in self.headers["Content-Length"]:
                # Proxies sometimes cause Content-Length headers to get
                # duplicated.  If all the values are identical then we can
                # use them but if they differ it's an error.
                pieces = re.split(r',\s*', self.headers["Content-Length"])
                if any(i != pieces[0] for i in pieces):
                    raise ValueError("Multiple unequal Content-Lengths: %r" %
                                     self.headers["Content-Length"])
                self.headers["Content-Length"] = pieces[0]
            content_length = int(self.headers["Content-Length"])
        else:
            content_length = None

        if self.headers['Icy-Metaint']:
            self._metaint = int(self.headers['Icy-Metaint'])

        self.meta = {
            'genre': self.headers.get('Icy-Genre', ''),
            'name': self.headers.get('Icy-Name', ''),
            'url': self.headers.get('Icy-Url', ''),
            'description': self.headers.get('Icy-Description', '')
        }

        if self.headers.get("Content-Encoding") == "gzip":
            self._decompressor = GzipDecompressor()

        print self.headers

        if self.headers.get("Transfer-Encoding") == "chunked":
            print "Chunked encoding :("
            # self.chunks = []
            # self.stream.read_until(b"\r\n", self._on_chunk_length)
            pass

        elif content_length is not None:
            self._net_stream.read_bytes(content_length, self._on_stream)

        else:
            self._net_stream.read_bytes(self._metaint if self._metaint is not None else self._chunk_size,
                                        self._on_stream)

    def _on_metadata(self, data):
        data = data.replace(b"\x00", "")

        def item(x):
            def clean(v):
                if v.startswith("'") and v.endswith("'"):
                    return v[1:-1]
                return v

            return (x[0], clean(x[1]))

        meta = dict((item(x.split('=', 1)) for x in data.split(';') if len(x) > 3))
        self.meta = meta

    def _on_mp3_data(self, data):
        self._data_buffer += data

        good_index = 0
        index = 0
        max = len(self._data_buffer)

        while index < max - 4:
            good, length = 0, 1
            if self._data_buffer.startswith('TAG', index):
                # ID3v1 tag
                good = 1
                length = 128
                print "ID3v1"

            elif self._data_buffer.startswith('ID3', index) and max - index > 9:
                # IV3v2 tag
                good = 1
                length = (ord(self._data_buffer[index + 6]) << 21) + \
                         (ord(self._data_buffer[index + 7]) << 14) + \
                         (ord(self._data_buffer[index + 8]) << 7) + \
                         (ord(self._data_buffer[index + 9]) + 10)
                print "ID3v2"

            elif self._data_buffer[index] == '\xff' and \
                    not self._data_buffer[index + 1] == '\xff' and \
                            (ord(self._data_buffer[index + 1]) & 224) == 224:
                # MP3 frames
                try:
                    hdr = mp3.frameheader(self._data_buffer, index)
                    length = mp3.framelen(hdr)
                    good = 1
                except mp3.MP3Error, e:
                    pass

            if good:
                if index + length <= max:
                    good_index = index + length
                    self._on_frame(self._data_buffer[index:good_index])

            index = index + length
        self._data_buffer = self._data_buffer[good_index:]

    def _on_frame(self, framedata):
        self.stream.push(AudioFrame(framedata))

    @tornado.gen.coroutine
    def _on_stream(self, data):
        datalen = len(data)
        self._totalbytes += datalen

        self._on_mp3_data(data)

        if self._metaint is not None:
            l = yield tornado.gen.Task(self._net_stream.read_bytes, 1)
            metadata_length = ord(l) * 16
            metadata = yield tornado.gen.Task(self._net_stream.read_bytes, metadata_length)
            self._on_metadata(metadata)

        self._net_stream.read_bytes(self._metaint if self._metaint is not None else self._chunk_size, self._on_stream)


if __name__ == '__main__':
    import signal
    import tornado.ioloop

    irs = [
        IceReceiver('http://mp3.nashe.ru/nashe-128.mp3'),
        IceReceiver('http://cast.loungefm.com.ua/chillout128?1372841633429.mp3'),
        # IceReceiver('http://air.radiorecord.ru:8102/tm_128'),
        IceReceiver('http://rupsy.ru:8000/psyprog')
    ]

    for i, ir in enumerate(irs):
        ir.use_meta(True)
        ir.record(open('/tmp/testrecord-%i.mp3' % i, 'wb'))
        ir.start()

    def handle_signal(signal, frame):
        print "Exiting"
        for ir in irs:
            ir.stop()

        tornado.ioloop.IOLoop.instance().add_callback_from_signal(lambda: tornado.ioloop.IOLoop.current().stop())

    signal.signal(signal.SIGHUP, handle_signal)
    signal.signal(signal.SIGINT, handle_signal)
    signal.signal(signal.SIGTERM, handle_signal)

    tornado.ioloop.IOLoop.instance().start()