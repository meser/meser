import time

import tornado.gen
from tornado.tcpserver import TCPServer

from meser.protocols.rtmp.proto.assembly import RTMPAssembler, RTMPDisassembler
from meser.protocols.rtmp.proto import constants
from meser.protocols.rtmp.proto.packets import Ping, BytesRead, Invoke
from meser.protocols.rtmp.proto.headers import RTMPHeader
from meser.protocols.rtmp.proto.handshake import IncomingHandshakeHandler
from meser.utils import inflection


class UnhandledInvokeError(Exception):
    """
    Unable to handle invoke (no such method).
    """
    code = constants.StatusCodes.NC_CALL_FAILED


class RTMPServer(TCPServer):
    def __init__(self, io_loop=None, ssl_options=None, max_buffer_size=None):
        super(RTMPServer, self).__init__(io_loop, ssl_options, max_buffer_size)
        self.applications = {}

    def add_application(self, name, app):
        self.applications[name] = app

    def handle_stream(self, stream, address):
        RTMPConnection(stream, address, self)


class RTMPConnection(object):
    class State:
        CONNECTING = 'connecting'  # Connection in progress
        HANDSHAKE_SEND = 'handshake-send'  # Handshake, 1st phase.
        HANDSHAKE_VERIFY = 'handshake-verify'  # Handshake, 2nd phase.
        RUNNING = 'running'  # Usual state of protocol: receiving-sending RTMP packets.

    def __init__(self, stream, address, server):
        """

        @type stream: BaseIOStream
        @param stream Tornado IOStream
        @param address tuple
        """
        print('Connection', stream, address)
        self.state = self.State.CONNECTING
        self.stream = stream
        self.address = address
        self.server = server

        self.stream.set_close_callback(self._on_close)

        self.input = RTMPDisassembler(constants.DEFAULT_CHUNK_SIZE)
        self.output = RTMPAssembler(constants.DEFAULT_CHUNK_SIZE, self.stream)

        self.invokeReplies = {}
        self.nextInvokeId = 0

        # connection timestamps
        self.peer_epoch = 0
        self.epoch = 0
        self.base_time = 0
        self.current_time = 0

        # ping
        # self.ping_evt = tornado.ioloop.PeriodicCallback(self.remove_dead_sessions, 5000)
        self.ping_active = 1
        self.ping_reset = 1

        self.acodecs = 0
        self.vcodecs = 0

        IncomingHandshakeHandler(self)

    def push_packet(self, packet):
        print('Packet send. Length:', self.output.push_packet(packet))

    def _first_ping(self):
        """
        Send first ping, usually after first connect.
        """
        self.push_packet(Ping(Ping.UNKNOWN_8, [0, 1, int(time.time()) & 0x7fffffff]))

    def _on_close(self):
        print('Client disconnected', self.address)

    # Reader
    @tornado.gen.coroutine
    def handle_stream_data(self, data=None):
        """
        Regular RTMP dataflow: stream of RTMP packets.

        Some bytes (L{data}) was received.

        @param data: bytes received
        @type data: C{str}
        """
        if data is None:
            return

        self.input.push_data(data)

        while True:
            packet = self.input.disassemble()
            if packet is None:
                return

            self.handle_packet(packet)

    # Packet handling
    def handle_packet(self, packet):
        """
        Dispatch received packet to some handler.

        @param packet: packet
        @type packet: L{Packet}
        """
        print('Packet: %r' % packet)
        handler = 'handle_' + inflection.underscore(packet.__class__.__name__)
        print('Packet handler:', handler)
        try:
            getattr(self, handler)(packet)
        except AttributeError:
            print('No handler for ', handler)

    def handle_ping(self, packet):
        """
        Handle incoming L{Ping} packets.

        @param packet: packet
        @type packet: L{Ping}
        """
        print('Ping:', packet)
        # stream buffer length, sending it to router, and sending buffer clear ping message
        if packet.event == Ping.CLIENT_BUFFER:
            self.push_packet(Ping(Ping.STREAM_CLEAR, [packet.data[0]]))
        # normal ping request
        elif packet.event == Ping.PING_CLIENT:
            self.push_packet(Ping(Ping.PONG_SERVER, packet.data))
        # normal pong
        elif packet.event == Ping.PONG_SERVER:
            pass  # we control last received time in L{dataReceived}
        # first ping ?
        elif packet.event == Ping.UNKNOWN_8:
            pass
        else:
            print("Unknown ping: %r" % packet)

    def handle_bytes_read(self, packet):
        """
        Handle incoming L{BytesRead} packets.

        @param packet: packet
        @type packet: L{BytesRead}
        """
        print('handleBytesRead', packet)

    def handle_invoke(self, packet):
        print('handleInvoke', packet)
        if packet.name in ["_result", "_error"]:
            if packet.id not in self.invokeReplies:
                # Got reply for not send invoke packet
                pass
            else:
                d = self.invokeReplies.pop(packet.id)
                if packet.name == "_result":
                    d.callback(packet.argv)
                else:
                    #d.errback(Status(**packet.argv[1]))
                    # Error
                    pass
            return

        handler = getattr(self, 'invoke_' + inflection.underscore(packet.name), None)
        if handler is None:
            handler = self.default_invoke_handler

        print('Invoke handler:', handler.__name__)

        handler(packet, *packet.argv)

    # Invokers
    def invoke(self, name, *args):
        """
        Perform invoke on other side of connection.

        @param name: method being invoked
        @type name: C{str}
        @param args: arguments for the call
        @type args: C{list}
        """
        packet = Invoke(name, (None, ) + args, self.nextInvokeId, RTMPHeader(timestamp=0, stream_id=0))
        d = self.invokeReplies[self.nextInvokeId] = None  # defer.Defered()
        self.nextInvokeId += 1
        self.push_packet(packet)
        return d

    def default_invoke_handler(self, packet, *args):
        """
        Dispatch invokes to current application.
        """
        assert self.application is not None

        handler = getattr(self.application, 'invoke_' + inflection.underscore(packet.name), None)

        if handler is None:
            raise UnhandledInvokeError(packet.name)

        def gotResult(result):
            return [None, result]

        # return defer.maybeDeferred(handler, self, *args[1:]).addCallback(gotResult)
        return [None, handler(self, *args[1:])]

    def invoke_connect(self, packet, connect_params, *args):
        """
        Connection to server application.

        @param packet: original Invoke packet
        @type packet: L{Invoke}
        @param connect_params: connection params
        @type connect_params: C{dict}
        """
        print("Connect to %r, Flash %s" % (connect_params['app'], connect_params['flashVer']))

        # self._first_ping()

        self.vcodecs = connect_params['videoCodecs']
        self.acodecs = connect_params['audioCodecs']

        self.flashVer =  connect_params['flashVer']

        connect_path = connect_params['app'].split('/')
        app_name = connect_path[0]
        del connect_path[0]

        self.application = self.server.applications[app_name](self, connect_path, *args)

        return
        # return self.application.connect(self, connect_path, *args).addCallback(connectOk)


class RTMPApplication(object):
    def __init__(self, connection, connection_path, *args):
        """

        @type connection: RTMPConnection
        @param connection RTMPConnection object
        @param connection_path list(str)
        """
        self.connection = connection
        self.path = connection_path
        # print(connection, connection_path, args)
        self.connection._first_ping()


if __name__ == '__main__':
    import tornado.process
    import tornado.netutil
    import tornado.ioloop

    IOLoop = tornado.ioloop.IOLoop.instance()

    import signal

    def handle_signal(signal, frame):
        print "Exiting"
        IOLoop.add_callback_from_signal(lambda: IOLoop.current().stop())

    signal.signal(signal.SIGHUP, handle_signal)
    signal.signal(signal.SIGINT, handle_signal)
    signal.signal(signal.SIGTERM, handle_signal)

    server = RTMPServer(IOLoop)
    server.add_application('push', RTMPApplication)
    server.add_application('live', RTMPApplication)
    server.listen(1935)
    IOLoop.start()