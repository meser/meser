import array
import random

import tornado.gen

from . import constants


GENUINE_FMS_KEY = [
    0x47, 0x65, 0x6e, 0x75, 0x69, 0x6e, 0x65, 0x20,
    0x41, 0x64, 0x6f, 0x62, 0x65, 0x20, 0x46, 0x6c,
    0x61, 0x73, 0x68, 0x20, 0x4d, 0x65, 0x64, 0x69,
    0x61, 0x20, 0x53, 0x65, 0x72, 0x76, 0x65, 0x72,
    0x20, 0x30, 0x30, 0x31,  # Genuine Adobe Flash Media Server 001
    0xf0, 0xee, 0xc2, 0x4a, 0x80, 0x68, 0xbe, 0xe8,
    0x2e, 0x00, 0xd0, 0xd1, 0x02, 0x9e, 0x7e, 0x57,
    0x6e, 0xec, 0x5d, 0x2d, 0x29, 0x80, 0x6f, 0xab,
    0x93, 0xb8, 0xe6, 0x36, 0xcf, 0xeb, 0x31, 0xae
]

GENUINE_FP_KEY = [
    0x47, 0x65, 0x6E, 0x75, 0x69, 0x6E, 0x65, 0x20,
    0x41, 0x64, 0x6F, 0x62, 0x65, 0x20, 0x46, 0x6C,
    0x61, 0x73, 0x68, 0x20, 0x50, 0x6C, 0x61, 0x79,
    0x65, 0x72, 0x20, 0x30, 0x30, 0x31,  # Genuine Adobe Flash Player 001
    0xF0, 0xEE, 0xC2, 0x4A, 0x80, 0x68, 0xBE, 0xE8,
    0x2E, 0x00, 0xD0, 0xD1, 0x02, 0x9E, 0x7E, 0x57,
    0x6E, 0xEC, 0x5D, 0x2D, 0x29, 0x80, 0x6F, 0xAB,
    0x93, 0xB8, 0xE6, 0x36, 0xCF, 0xEB, 0x31, 0xAE
]

# Modulus bytes from flazr
DH_MODULUS_BYTES = [
    0xff, 0xff, 0xff, 0xff, 0xff,
    0xff, 0xff, 0xff, 0xc9, 0x0f, 0xda, 0xa2, 0x21,
    0x68, 0xc2, 0x34, 0xc4, 0xc6, 0x62, 0x8b, 0x80,
    0xdc, 0x1c, 0xd1, 0x29, 0x02, 0x4e, 0x08, 0x8a,
    0x67, 0xcc, 0x74, 0x02, 0x0b, 0xbe, 0xa6, 0x3b,
    0x13, 0x9b, 0x22, 0x51, 0x4a, 0x08, 0x79, 0x8e,
    0x34, 0x04, 0xdd, 0xef, 0x95, 0x19, 0xb3, 0xcd,
    0x3a, 0x43, 0x1b, 0x30, 0x2b, 0x0a, 0x6d, 0xf2,
    0x5f, 0x14, 0x37, 0x4f, 0xe1, 0x35, 0x6d, 0x6d,
    0x51, 0xc2, 0x45, 0xe4, 0x85, 0xb5, 0x76, 0x62,
    0x5e, 0x7e, 0xc6, 0xf4, 0x4c, 0x42, 0xe9, 0xa6,
    0x37, 0xed, 0x6b, 0x0b, 0xff, 0x5c, 0xb6, 0xf4,
    0x06, 0xb7, 0xed, 0xee, 0x38, 0x6b, 0xfb, 0x5a,
    0x89, 0x9f, 0xa5, 0xae, 0x9f, 0x24, 0x11, 0x7c,
    0x4b, 0x1f, 0xe6, 0x49, 0x28, 0x66, 0x51, 0xec,
    0xe6, 0x53, 0x81, 0xff, 0xff, 0xff, 0xff, 0xff,
    0xff, 0xff, 0xff
]


class Handshake(object):
    def __init__(self):
        self.HANDSHAKE_PAD_BYTES = None
        self.handshake_bytes = b''
        self.createHandshakeBytes()

    def generateUnversionedHandshake(self):
        buffer = bytes()
        if not self.HANDSHAKE_PAD_BYTES:
            self.HANDSHAKE_PAD_BYTES = array.array('B')
            for i in range(constants.HANDSHAKE_SIZE - 4):
                self.HANDSHAKE_PAD_BYTES.append(0x00)

        buffer += bytes(constants.RTMP_NON_ENCRYPTED)
        buffer += 123455

    def createHandshakeBytes(self):
        raise NotImplementedError


    def generateKeyPair(self):
        pass


class IncomingHandshakeHandler(Handshake):
    def __init__(self, connection):
        """
        Incoming handshake request handler

        @param connection:
        @type connection: RTMPConnection
        """
        self.version = 0
        self.connection = connection
        super(IncomingHandshakeHandler, self).__init__()

        self.client_handshake_bytes = b''

        self.connection.stream.read_bytes(1, self.handle_version)  # C0

    def createHandshakeBytes(self):
        handshake_bytes = [chr(random.randint(0, 255)) for x in xrange(constants.HANDSHAKE_SIZE)]

        # Timestamp
        handshake_bytes[0] = chr(0x0)
        handshake_bytes[1] = chr(0x0)
        handshake_bytes[2] = chr(0x0)
        handshake_bytes[3] = chr(0x0)

        # Version
        handshake_bytes[4] = chr(0x1)
        handshake_bytes[5] = chr(0x2)
        handshake_bytes[6] = chr(0x3)
        handshake_bytes[7] = chr(0x4)

        self.handshake_bytes = bytes(b''.join(handshake_bytes))

    # Handlers
    @tornado.gen.coroutine
    def handle_version(self, version):
        self.version = ord(version)
        self.connection.stream.read_bytes(constants.HANDSHAKE_SIZE, self.handle_exchange)  # C1

    @tornado.gen.coroutine
    def handle_exchange(self, data):
        self.connection.stream.write(chr(0x3))  # S0 Version 3

        # And what I need to do with it?
        epoch = [ord(x) for x in bytes(data[:4])]
        version = [ord(x) for x in bytes(data[4:8])]

        self.client_handshake_bytes = data

        self.connection.stream.write(self.handshake_bytes)  # S1
        self.connection.stream.read_bytes(constants.HANDSHAKE_SIZE, self.handle_verify)  # C2

    @tornado.gen.coroutine
    def handle_verify(self, data):
        assert data == self.handshake_bytes

        self.connection.stream.write(self.client_handshake_bytes)  # S2

        self.connection.stream.read_until_close(self.connection.handle_stream_data,
                                                streaming_callback=self.connection.handle_stream_data)


class OutgoingHandshakeHandler(Handshake):
    def __init__(self, connection):
        """
        Outgoing handshake request handler

        @param connection:
        @type connection: RTMPReceiverConnection
        """
        self.connection = connection
        super(OutgoingHandshakeHandler, self).__init__()

        self.version = 0
        self.server_handshake_bytes = b''

        self.handle_c0()

    def createHandshakeBytes(self):
        handshake_bytes = [chr(random.randint(0, 255)) for x in xrange(constants.HANDSHAKE_SIZE)]

        # Timestamp
        handshake_bytes[0] = chr(0x0)
        handshake_bytes[1] = chr(0x0)
        handshake_bytes[2] = chr(0x0)
        handshake_bytes[3] = chr(0x0)

        # Version
        handshake_bytes[4] = chr(0x1)
        handshake_bytes[5] = chr(0x2)
        handshake_bytes[6] = chr(0x3)
        handshake_bytes[7] = chr(0x4)

        self.handshake_bytes = bytes(b''.join(handshake_bytes))

    # Handlers
    @tornado.gen.coroutine
    def handle_c0(self):
        self.connection.stream.write(chr(3), self.handle_c1)

    @tornado.gen.coroutine
    def handle_c1(self):
        self.connection.stream.write(self.handshake_bytes)
        self.connection.stream.read_bytes(1, self.handle_s0)

    @tornado.gen.coroutine
    def handle_s0(self, version):
        self.version = ord(version)
        self.connection.stream.read_bytes(constants.HANDSHAKE_SIZE, self.handle_s1_c2)

    @tornado.gen.coroutine
    def handle_s1_c2(self, data):
        epoch = [ord(x) for x in bytes(data[:4])]
        version = [ord(x) for x in bytes(data[4:8])]

        self.server_handshake_bytes = data
        self.connection.stream.write(self.server_handshake_bytes, self.handle_s2)

    @tornado.gen.coroutine
    def handle_s2(self, data):
        assert data == self.handshake_bytes

        # Do anything to restore context execution to connection
