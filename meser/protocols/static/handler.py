import os
import time
import random
import mimetypes

import tornado.web
import tornado.gen
import tornado.httpclient

from meser.core.app import Application
from meser.core.defaults.handlers import HttpRequestHandler


class StaticFileHandler(HttpRequestHandler):
    def initialize(self, *args, **kwargs):
        self._origin = kwargs.get('origin', None)
        self._webroot = kwargs.get('webroot', '')
        self._root = kwargs.get('root', None)
        self._max_age = int(kwargs.get('max_age', 0))

    @tornado.web.asynchronous
    def head(self, path, *args, **kwargs):
        print('HEAD')
        self.get(path, do_not_read_body=True)

    @tornado.web.asynchronous
    @tornado.gen.engine
    def get(self, path, do_not_read_body=False, *args, **kwargs):
        norm_path = self._normalize_webroot_path(path)
        info = yield tornado.gen.Task(self._get_file_info, norm_path)
        info = info.kwargs
        if 'error' in info:
            self.send_error(info['error'])

        self.set_header('Accept-Ranges', 'bytes')

        range = None
        range_type = None
        if 'range' in self.request.headers:
            r = self.request.headers['range'].split('=', 1)
            range_type = r[0]
            range = list(map(int, r[1].split('-', 1)))

        if range:
            self.set_status(206)
            self.set_header('Content-Length', (range[1]-range[0]))
            self.set_header('Content-Range', '%s %i-%i/%i' % (range_type, range[0], range[1], info['f_size']))

        if not do_not_read_body:
            if info['f_size'] < 4096:
                with open(info['f_path'], 'rb') as f:
                    if range:
                        f.seek(range[0])
                        self.write(f.read(range[1]-range[0]))
                    else:
                        self.write(f.read())
            else:
                # @TODO: Make async
                with open(info['f_path'], 'rb') as f:
                    self.write(f.read())
            self.finish()

    # ##################################
    # Private functions
    # ##################################
    @tornado.web.asynchronous
    @tornado.gen.engine
    def _get_file_info(self, path, callback):
        if not self._root:
            raise tornado.web.HTTPError(500, 'Files root is not configured.')

        full_path = os.path.join(self._root, path)
        if not os.path.exists(full_path):
            resp = yield tornado.gen.Task(self._load_from_origin, path)

            resp = resp.args
            if resp[0]:
                response = resp[1]
                parent_dir = os.path.dirname(full_path)

                if not os.path.exists(parent_dir):
                    try:
                        os.makedirs(parent_dir)
                    except Exception as e:
                        pass

                with open(full_path, 'wb') as f:
                    # @TODO: make async
                    # response.buffer.read_until_close(f.write, f.write)
                    f.write(response.buffer.read())

            else:
                callback(error=404)
                return

            # Recall myself
            callback((yield tornado.gen.Task(self._get_file_info, path)))
            return
        else:
            c_time = os.path.getctime(full_path)

            # Cache control
            if self._max_age > 0:
                if time.time() - c_time < self._max_age:
                    os.unlink(full_path)
                    callback((yield tornado.gen.Task(self._get_file_info, path)))
                    return

            m_time = os.path.getmtime(full_path)
            f_size = os.path.getsize(full_path)
            f_mime = mimetypes.guess_type(full_path)

            self.set_header('Content-Length', f_size)
            self.set_header('Content-Type', f_mime[0])

            callback(c_time=c_time, f_size=f_size, f_mime=f_mime, f_path=full_path)
            return

        # callback(error=500)

    @tornado.web.asynchronous
    @tornado.gen.engine
    def _load_from_origin(self, path, callback):
        origin = self._origin if type(self._origin) == str else random.choice(self._origin)
        http_client = tornado.httpclient.HTTPClient()
        response = yield tornado.gen.Task(http_client.fetch, "%s/%s" % (self._normalize_uri(origin), self._normalize_uri(path)))
        if response.error:
            callback(False, response)
        callback(True, response)

    def _normalize_uri(self, uri):
        if uri[0] == '/':
            uri = uri[1:]
        if uri[-1] == '/':
            uri = uri[:-1]
        return uri

    def _normalize_webroot_path(self, path):
        if len(self._webroot) and self._webroot in path:
            return path[len(self._webroot):]
        return path