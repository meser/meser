import os
import sys
from meser.utils.proctitle import set_proc_name


class BaseService(object):
    def __init__(self, application):
        self.application = application
        self._pid = os.getpid()
        set_proc_name(self.get_service_name())

    def get_service_name(self):
        raise NotImplementedError

    def start(self):
        pass

    def stop(self):
        pass